#include "lib/fibonacci_calculator.h"

#include <iostream>

bool test_two_terms() {
	auto actual = FibonacciCalculator{2}.calculate();
	std::cout << actual << std::endl;
	if (actual.compare("0, 1") == 0) {
		return true;
	}
	return false;
}

bool test_three_terms() {
	auto actual = FibonacciCalculator{3}.calculate();
	std::cout << actual << std::endl;
	if (actual.compare("0, 1, 1") == 0) {
		return true;
	}
	return false;
}

bool test_four_terms() {
	auto actual = FibonacciCalculator{4}.calculate();
	std::cout << actual << std::endl;
	if (actual.compare("0, 1, 1, 2") == 0) {
		return true;
	}
	return false;
}

bool test_five_terms() {
	auto actual = FibonacciCalculator{5}.calculate();
	std::cout << actual << std::endl;
	if (actual.compare("0, 1, 1, 2, 3") == 0) {
		return true;
	}
	return false;
}

int main() {
	return test_two_terms() & test_three_terms() & test_four_terms() & test_five_terms();
}
