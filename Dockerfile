FROM ubuntu:16.04

# Copy files
COPY . /root/cc_demo

# Install Bazel build system
RUN apt-get update
RUN apt-get install -y software-properties-common curl openjdk-8-jdk git make cmake gcc
RUN echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list
RUN curl https://bazel.build/bazel-release.pub.gpg | apt-key add -
RUN apt-get update
RUN apt-get install -y bazel
RUN apt-get install -y --only-upgrade bazel

# Build 
RUN cd $HOME/cc_demo && $HOME/cc_demo/scripts/build.sh
CMD cd $HOME/cc_demo && $HOME/cc_demo/scripts/run.sh
