#ifndef _MAIN_FIBONACCI_CALCULATOR_H_
#define _MAIN_FIBONACCI_CALCULATOR_H_

#include <string>

class FibonacciCalculator {
private:
	int n;
public:
	FibonacciCalculator(int n) : n{n} { }
	std::string calculate();
};

#endif
