#include "fibonacci_calculator.h"

#include <cstdlib>
#include <sstream>

std::string FibonacciCalculator::calculate() {
        auto oss = std::ostringstream{};
        auto first = 0;
        auto second = 1;
        auto next = 0;
        for (auto i = 1; i <= n; ++i) {
                if (i == 1) {
                        oss << first << " ";
                        continue;
                }
                if (i == 2) {
                        oss << second << " ";
                        continue;
                }
                next = first + second;
                first = second;
                second = next;
                oss << next << " ";
        }
        return oss.str();
}
