# Pull and build external dependencies
rm -rf pistache
git clone https://github.com/oktal/pistache
cd pistache
git submodule update --init
mkdir build
cd build
cmake ../
make
make install

cd ..
echo '
cc_library(
  name = "pistache",
  srcs = [":build/src/libpistache.so", ":build/src/libpistache.so.0"],
  hdrs = glob([":include/**/*.h"]),
  visibility = ["//visibility:public"],
)' >> BUILD

cd $HOME/cc_demo

# Build our code
bazel build //...
bazel test //...
