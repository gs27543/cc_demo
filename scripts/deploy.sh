echo "[*] DEPLOY : Logging in to Azure..."
appId="0b576cc6-2ffa-45ce-9591-1f826c4e5ebb"
password="DhBByfqCLK5pAhca"
tenant="1d1cd40d-9830-496f-89cd-3e4903879726"
az login --service-principal -u $appId --password $password --tenant $tenant
az acr login --name ccDemoMvp
# {
#  "appId": "0b576cc6-2ffa-45ce-9591-1f826c4e5ebb",
#  "displayName": "CCDemoServicePrincipal",
#  "name": "http://CCDemoServicePrincipal",
#  "password": "DhBByfqCLK5pAhca",
#  "tenant": "1d1cd40d-9830-496f-89cd-3e4903879726"
# }
echo "[*] DEPLOY : Removing existing Docker image..."
sudo docker rmi $(docker images -q) --force
echo "[*] DEPLOY : Building Docker image..."
sudo docker build . -t cc-demo-app --label cc-demo-app
id=$(sudo docker images --filter "label=cc-demo-app" --format "{{.ID}}" | head -n 1)
echo $id
echo "[*] DEPLOY : Tagging Docker image..."
sudo docker tag cc-demo-app ccdemomvp.azurecr.io/cc-demo-app:$id
echo "[*] DEPLOY : Pushing Docker image..."
sudo docker push ccdemomvp.azurecr.io/cc-demo-app:$id
echo "[*] DEPLOY : Getting password..."
password=$(az acr credential show --name ccDemoMvp --query "passwords[0].value")
echo "[*] DEPLOY : Creating Azure container..."
cmd="az container create --resource-group ccDemoApp --name cc-demo-app --image ccdemomvp.azurecr.io/cc-demo-app:$id --cpu 1 --memory 1 --ip-address public --ports 80 --registry-username ccdemomvp --registry-password $password"
eval $cmd
echo "[*] DEPLOY : Done (!)"
