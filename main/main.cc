#include <cstdlib>
#include <string> 

#include "lib/fibonacci_calculator.h"
#include "pistache/endpoint.h"

using namespace Pistache;

class FibonacciHandler: public Http::Handler {
public:

    HTTP_PROTOTYPE(FibonacciHandler)

    void onRequest(const Http::Request& request, Http::ResponseWriter response) {
    	std::srand(std::time(nullptr));
	auto n = std::rand() % 16 + 1;	
	auto fib = FibonacciCalculator{n};
        response.send(Http::Code::Ok, fib.calculate());
    }
};

int main() {
    Address addr(Ipv4::any(), Port(80));
    auto opts = Http::Endpoint::options().threads(1);

    Http::Endpoint server(addr);
    server.init(opts);
    server.setHandler(Http::make_handler<FibonacciHandler>());
    server.serve();

    server.shutdown();
}
